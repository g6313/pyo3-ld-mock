pub mod pkg;
use aho_corasick::{AhoCorasickKind, MatchKind};
use pyo3::exceptions::PyTypeError;
use pyo3::prelude::*;
use pyo3::types::PyString;
use pyo3::wrap_pyfunction;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::path::Path;


#[pyfunction]
fn wrap_lookup_write(
    _py: Python<'_>,
    source: Vec<String>,
    destination: Vec<String>,
    path: &str,
) -> PyResult<String> {
    let lookup = pkg::replacer::Lookup::new(source, destination);
    lookup.write_to_path(path);
    Ok("ALL ITS OK".to_string())
}

#[pyfunction]
fn wrap_lookup_load(_py: Python<'_>, path: &str) -> PyResult<(Vec<String>, Vec<String>)> {
    let lookup = pkg::replacer::Lookup::load_path(path);
    Ok(lookup.get_data())
}


#[pymodule]
fn pytextrust(_py: Python, m: &PyModule) -> PyResult<()> {
    pyo3_log::init();

    m.add_function(wrap_pyfunction!(wrap_lookup_write, m)?)?;
    m.add_function(wrap_pyfunction!(wrap_lookup_load, m)?)?;


    Ok(())
}
