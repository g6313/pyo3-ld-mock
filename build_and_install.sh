docker run --rm --entrypoint rustc ghcr.io/pyo3/maturin:latest --version
echo "LINTING WITH CARGO CLIPPY"
docker run --rm -t -e CARGO_HOME=.project_cargo -e "TERM=xterm-256color" \
    -v $(pwd):/io --entrypoint cargo ghcr.io/pyo3/maturin:latest clippy --manifest-path Cargo.toml
echo "RUST TESTING"
docker run --rm -t -e CARGO_HOME=.project_cargo -e "TERM=xterm-256color" \
    -v $(pwd):/io --entrypoint cargo ghcr.io/pyo3/maturin:latest test --no-fail-fast --release --manifest-path Cargo.toml


echo "CONTAINERIZED BUILD"
docker run --rm -t -v $(pwd)/target/wheels:/home -w /home \
    python:3.9-slim-bookworm \
    /bin/bash -c "rm *.whl"
docker run --rm -t \
    -v $(pwd):/io ghcr.io/pyo3/maturin:latest \
    build --release -i python3.9 --manylinux 2014
