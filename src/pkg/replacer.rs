use bincode;
use log::debug;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs;
use std::io::{BufReader, Read};
use std::path::Path;


#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Lookup {
    src: Vec<String>,
    dst: Vec<String>,
}

impl Lookup {
    pub fn new(src: Vec<String>, dst: Vec<String>) -> Self {
        Self { src, dst }
    }

    pub fn load_path(path: &str) -> Self {
        debug!("Loading Lookup table at {path}");
        let path: &Path = Path::new(path);
        let file: fs::File = fs::File::open(path).unwrap();
        let metadata = file.metadata();
        let mut reader = BufReader::new(file);

        let mut buffer: Vec<u8> = match metadata {
            Ok(meta) => Vec::with_capacity(meta.len() as usize),
            Err(_) => Vec::new(),
        };
        reader.read_to_end(&mut buffer).unwrap();
        let result: Lookup = bincode::deserialize(&buffer).unwrap();
        debug!(
            "Loaded Lookup table with {} src and {} dst elements",
            result.src.len(),
            result.dst.len()
        );
        result
    }

    pub fn write_to_path(&self, path: &str) {
        let path: &Path = Path::new(path);
        let encoded: Vec<u8> = bincode::serialize(&self).unwrap();
        let _ = fs::write(path, encoded);
    }

    pub fn get_data(self) -> (Vec<String>, Vec<String>) {
        (self.src, self.dst)
    }

    pub fn src(&self) -> &Vec<String> {
        &self.src
    }

    pub fn dst(&self) -> &Vec<String> {
        &self.dst
    }
}

///Simple transformation of a string if it is in a hashmap, else return original string
#[derive(Clone, Serialize, Deserialize, PartialEq, Debug)]
pub struct MapLookup {
    data: HashMap<String, String>,
}

impl MapLookup {
    pub fn new(src: Vec<String>, dst: Vec<String>) -> Self {
        let data: HashMap<String, String> = src.into_iter().zip(dst.into_iter()).collect();
        Self { data }
    }

    pub fn write_to_path(&self, path: &str) {
        let path: &Path = Path::new(path);
        let encoded: Vec<u8> = bincode::serialize(&self).unwrap();
        fs::write(path, encoded).expect("Unable to write file");
    }

    pub fn load_path(path: &str) -> Self {
        debug!("Loading Lookup table at {path}");
        let path: &Path = Path::new(path);
        let file: fs::File = fs::File::open(path).unwrap();
        let metadata = file.metadata();
        let mut reader = BufReader::new(file);

        let mut buffer: Vec<u8> = match metadata {
            Ok(meta) => Vec::with_capacity(meta.len() as usize),
            Err(_) => Vec::new(),
        };
        reader.read_to_end(&mut buffer).unwrap();
        let result: MapLookup = bincode::deserialize(&buffer).unwrap();
        debug!("Loaded Lookup table with {} elements", result.data.len(),);
        result
    }

    pub fn apply<'a>(&'a self, src: &'a str) -> &'a str {
        match self.data.get(src) {
            Some(val) => val.as_ref(),
            None => src,
        }
    }

    pub fn get_data(self) -> HashMap<String, String> {
        self.data
    }
}

impl From<MapLookup> for Lookup {
    fn from(val: MapLookup) -> Self {
        let (src, dst): (Vec<String>, Vec<String>) = val.data.into_iter().unzip();
        Lookup::new(src, dst)
    }
}

impl From<Lookup> for MapLookup {
    fn from(val: Lookup) -> Self {
        MapLookup::new(val.src, val.dst)
    }
}
