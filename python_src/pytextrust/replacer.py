from pytextrust.pytextrust import wrap_lookup_write, wrap_lookup_load
from typing import List
from enum import Enum



def load_lookup(path: str):
    src, dst = wrap_lookup_load(path)
    return src, dst


def write_lookup(src_list: List[str], dst_list: List[str], path: str):
    wrap_lookup_write(source=src_list, destination=dst_list, path=path)
